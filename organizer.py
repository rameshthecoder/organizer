import sys, os
import time
import argparse
import urllib, mimetypes
import shutil

from PyQt5.QtCore       import *
from PyQt5.QtGui        import *
from PyQt5.QtQuick      import *
from PyQt5.QtQml        import *
from PyQt5.QtMultimedia import *
from PyQt5.QtPrintSupport import *
from PyQt5.QtWidgets    import *
from os.path import expanduser


class Application( QObject ):

    myPySignal = pyqtSignal( int )

    destination_directories = {
        "audio": "audiosDirectory",
        "image": "imagesDirectory",
        "text": "documentsDirectory",
        "video": "videosDirectory",
        "others":  "othersDirectory"
    }

    defDirs = {
        "audio" : "/Audios",
        "image" : "/Images",
        "text"  :  "/Documents",
        "video" :    "/Videos",
        "others":    "/Others"    
    }

    # url = urllib.pathname2url('Upload.xml') 
    # print mimetypes.guess_type(url)

    def __init__( self ):
        QObject.__init__( self )
        self.app = QApplication( sys.argv )
        engine = QQmlEngine()
        engine.quit.connect( self.quit )
        self.app.lastWindowClosed.connect( self.quit )

        appDirPath = os.path.dirname( os.path.abspath( __file__ ) )
        rootContext = engine.rootContext()
        rootContext.setContextProperty( 'controller', self )
        rootContext.setContextProperty( 'appPath', appDirPath.replace( "\\", "/" ) + "/" )

        parser = argparse.ArgumentParser()
        parser.add_argument( "-f", action='store_true', help="fullscreen" )
        # parser.add_argument( "--ip", type=str, default="127.0.0.1", help="The ip of RGServer machine." )
        args = parser.parse_args()

        # self._ip = args.ip

        windowComp = QQmlComponent( engine, QUrl( 'qml/main.qml' ) )
        self.window = windowComp.create()
        if not self.window:
            print( "Unable to load qml: {0}".format( windowComp.errorString() ) )
            return
        iconPath = os.path.join( appDirPath, os.path.normcase( './qml/images/icon' ) )
        self.window.setIcon( QIcon( iconPath ) )
        self.settings = QSettings("settings.ini", QSettings.IniFormat)

        home = expanduser("~")


        # self.settings.setValue(self.destination_directories["audio"], home + "/Audios")
        # self.settings.setValue(self.destination_directories["image"], home + "/Images")
        # self.settings.setValue(self.destination_directories["text"], home + "/Documents")
        # self.settings.setValue(self.destination_directories["video"], home + "/Videos")
        # self.settings.setValue(self.destination_directories["others"], home + "/Others")

        if args.f:
            self.window.showFullScreen()
        else:
            self.window.show()
        self.app.exec()

    @pyqtSlot( int, str )
    def myPySlot( self, x, a ):
        print( x, a )

    @pyqtSlot(str)
    def sourceFolderChosen(self, sourceFolder):
        for fileName in os.listdir(sourceFolder):
            filePath = sourceFolder + "/" + fileName
            self.moveFile(filePath, mimetypes.guess_type(fileName)[0])

    def moveFile(self, fileName, fileMimeType):
        print(fileMimeType)
        if fileMimeType is None:
            return
        fileType = fileMimeType.split("/")[0]
        self.settings.sync()
        destDir = self.settings.value(self.destination_directories["others"], self.defDirs["others"] )
        try:
            destDir = self.settings.value(self.destination_directories[fileType], self.defDirs[fileType])
        except Exception as e:
            print(e)        

        try:
            os.makedirs(destDir)
        except Exception as e:    
            pass
        print("Destination directory: ", destDir)
        print("File name: ", fileName)
        print("File type:", fileType)
        shutil.move(fileName,destDir)


    def cleanup( self ):
        pass

    def quit( self ):
        self.cleanup()
        self.app.quit()

if __name__ == "__main__":
    app = Application()
#   myPySignal.emit()