import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Controls 2.3
import QtQuick.Dialogs 1.0

Window {
    id: root
    visible: true
    width: 400
    height: 300
    title: qsTr("Hello World")

    FileDialog {
        id: fileDialog
        title: "Please choose a file"
        folder: appPath
        selectFolder: true
        onAccepted: {
            console.log("You chose: " + fileDialog.fileUrls)
            textEditFilePath.text = fileDialog.fileUrls[0].toString().replace("file://","")
        }
        onRejected: {
            console.log("Canceled")
        }
    }

    Button {
        id: buttonChoose
        x: 33
        y: 139
        width: 100
        height: 23
        text: qsTr("Choose")
        onClicked: {
            fileDialog.visible = true
        }
    }

    TextEdit {  
        id: textEditFilePath
        x: 33
        y: 84
        width: 278
        height: 20
        text: "/home/ramesh/Downloads/"
        font.pixelSize: 12
    }

    Button {
        id: buttonOrganize
        x: 33
        y: 184
        width: 100
        height: 23
        text: qsTr("Organize")
        onClicked: {
            controller.sourceFolderChosen(textEditFilePath.text)
        }
    }

    Child {
        id: child
    }

    Button {
        id: buttonTest
        x: 33
        y: 232
        text: qsTr("Test")
        onClicked: {
            child.show()
            /*var component = Qt.createComponent("child.qml")
            var window    = component.createObject(root)
            component.onStatusChanged = function() {
                    if (component.status == Component.Ready) {
                        window.show()
                    }
            }*/
        }
    }
}
